if exists("syntax_on")
	syntax reset
endif

exec "source " . expand('<sfile>:p:h') . "/default-light.vim"

" Sets number of colors
set t_Co=16

let colors_name = "dim"

" In diffs, added lines are green, changed lines are yellow, deleted lines are
" red, and changed text (within a changed line) is bright yellow and bold.
highlight DiffAdd        ctermfg=0    ctermbg=2
highlight DiffChange     ctermfg=none ctermbg=7
highlight DiffDelete     ctermfg=0    ctermbg=1
highlight DiffText       ctermfg=0    ctermbg=11

" Highlight visual mode in white
highlight Visual                      ctermbg=7

" Highlight search matches in black, with a yellow background
highlight Search         ctermfg=none ctermbg=11

" Highlight quick fix line in white not yellow
highlight QuickFixLine                ctermbg=7

" Make comments italic
highlight Comment                                  cterm=italic

" Make important parts of the code bold
highlight PreProc                                  cterm=bold
highlight Statement        ctermfg=6               cterm=bold
highlight Type                                     cterm=bold

" Make line numbers yellow
highlight LineNr           ctermfg=3               cterm=italic
highlight CursorLineNr     ctermfg=3               cterm=none

" Highlight only important spelling errors by underlining
highlight clear SpellBad
highlight SpellBad                                 cterm=underline
highlight clear SpellCap
highlight SpellCap                                 cterm=underline
highlight clear SpellLocal
highlight clear SpellRare

" Make matching parentheses more discreet to not be confused with the cursor
highlight MatchParen     ctermbg=NONE ctermfg=6    cterm=bold

" Make the character set in folds and splits more minimal
set fillchars+=vert:\│
set fillchars+=fold:\ 

" Dim line numbers, color columns, the status line, splits, sign columns, special keys and the tab line
highlight ColorColumn    ctermfg=8    ctermbg=7
highlight Folded                      ctermbg=7
highlight FoldColumn     ctermfg=8    ctermbg=7
highlight Pmenu          ctermfg=0    ctermbg=7
highlight PmenuSel       ctermfg=7    ctermbg=2
highlight PmenuSbar                   ctermbg=7
highlight PmenuThumb                  ctermbg=0
highlight SpellCap       ctermfg=8    ctermbg=7
highlight StatusLine     ctermfg=NONE ctermbg=7    cterm=bold
highlight StatusLineNC   ctermfg=NONE ctermbg=7    cterm=NONE
highlight Wildmenu       ctermfg=NONE ctermbg=2    cterm=NONE
highlight VertSplit      ctermfg=7    ctermbg=NONE cterm=NONE
highlight SignColumn                  ctermbg=NONE
highlight SpecialKey     ctermfg=7
highlight Conceal        ctermfg=NONE ctermbg=7    cterm=NONE
highlight TabLine        ctermfg=NONE ctermbg=7    cterm=NONE
highlight TabLineFill    ctermfg=7
highlight TabLineSel     ctermfg=NONE ctermbg=NONE cterm=bold
highlight Cursorline                  ctermbg=7    cterm=NONE
