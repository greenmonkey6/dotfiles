" Do not behave like vi
set nocompatible

" Enable file type plugins
filetype plugin on
filetype indent on


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Plugins
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

call plug#begin('~/.vim/plugged')
Plug 'calviken/vim-gdscript3'
Plug 'junegunn/fzf.vim'
Plug 'kovetskiy/sxhkd-vim'
call plug#end()


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Auto Complete
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => fzf
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Search workspace files
nnoremap <silent> <space>f  :<C-u>Files<cr>
" Go to buffer
nnoremap <leader>b :<C-u>Buffers<cr>
" Don't use the overlay window
let g:fzf_layout = { 'down': '40%' }


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Set line numbers
set number
set relativenumber

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Height of the command bar
set cmdheight=1

" A buffer becomes hidden when it is abandoned
set hid

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch 

" Don't redraw while executing macros (good performance config)
set lazyredraw 

" For regular expressions turn magic on
set magic

" Show matching brackets when text indicator is over them
set showmatch 
" How many tenths of a second to blink when matching brackets
set mat=1

" Enable spell checking
set spell
" Set spell languages to (American) English and German
set spelllang=en_us,de_de

" Enable conceal
autocmd FileType tex set conceallevel=2
let g:tex_conceal="abdgm"

" Search into subfolders (on tab complete)
set path=**

" Display all matching files on tab complete
set wildmenu


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Enables syntax highlighting
syntax enable

" Set UTF-8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac

colorscheme dim


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Files, backups and undo
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Be smart when using tabs ;)
set smarttab

" Sets tab width (depending on the screen size)
if winwidth('%') < 80
	set shiftwidth=2
	set tabstop=2
else
	set shiftwidth=4
	set tabstop=4
endif

set noexpandtab
autocmd filetype haskell set expandtab
set autoindent "Auto indent
set smartindent "Smart indent
set wrap "Wrap lines

" Sets symbols for spaces, tabs etc.
set listchars=tab:▸\ ,trail:·,nbsp:_
set list

" Smart line wrap
set breakindent


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Moving around, tabs, windows and buffers
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Return to last edit position when opening files
autocmd BufReadPost *
	\ if line("'\"") > 0 && line("'\"") <= line("$") |
	\ 	exe "normal! g`\"" |
	\ endif
" Remember info about open buffers on close
set viminfo^=%

" Move between tabs
nnoremap J gT
nnoremap K gt


""""""""""""""""""""""""""""""
" => Status line
""""""""""""""""""""""""""""""

" Always show the status line
set laststatus=2

" Never show current position
set noruler


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Snippets
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" LaTeX

" Lets vim use 'tex' as standard file type for unknown tex flavors
let g:tex_flavor = 'tex'

autocmd FileType tex nnoremap <F5> :!pdflatex -shell-escape %<CR>
autocmd FileType tex nnoremap <F7> :!zathura %:r.pdf 2>/dev/null &disown<CR><CR>

" Markdown
autocmd FileType markdown nnoremap <F5> :!clear; clear; pandoc -o %:r.html %<CR>
"autocmd FileType markdown nnoremap <F7> :!chromium --app=file:///$(pwd)/%:r.html 2>/dev/null &disown<CR><CR>
autocmd FileType markdown nnoremap <F7> :!chromium --new-window %:r.html 2>/dev/null &disown<CR><CR>

" C++
autocmd FileType cpp nnoremap <F5> :make<CR>
autocmd FileType cpp nnoremap <F7> :make run<CR>



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Explorer (netrw)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:netrw_banner = 0


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Misc
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Remove the Windows ^M - when the encodings gets messed up
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Use X clipboard
set clipboard=unnamedplus

" Do not store current directory and global/local variables in a session
set ssop-=options
set ssop-=curdir

" Splits open at the bottom and right
set splitbelow
set splitright
