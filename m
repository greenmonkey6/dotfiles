#!/bin/sh

# Lets script stop on first error
set -e

machine_branch="$(hostname)"
if [ "$machine_branch" = "localhost" ]; then
	if [ "$USER" = "tobi" ]; then
		machine_branch="archdroid"
	else
		if [ "$(getprop | grep "ro.product.device" | cut -d ' ' -f 2)" = "[Nord]" ]; then
			machine_branch="nord"
		else
			machine_branch="termux"
		fi
	fi
fi

if [ "$1" = "master" ] || [ "$1" = "m" ]; then
	case="0"
elif [ "$1" = "commit" ] || [ "$1" = "c" ]; then
	case="10"
elif [ "$1" = "local" ] || [ "$1" = "l" ]; then
	case="20"
elif [ "$1" = "update" ] || [ "$1" = "u" ]; then
	case="30"
elif [ "$1" = "rebase" ] || [ "$1" = "r" ]; then
	case="40"
elif [ "$1" = "--help" ] || [ "$1" = "-h" ] || [ "$1" = "" ]; then
	echo "Usage: ch.sh COMMAND"
	echo "       ch.sh NUMBER"
	echo
	echo "COMMANDS"
	echo "  m, master  Switch to master branch"
	echo "  c, commit  Commit changes added to master branch and rebase local machine branch"
	echo "  l, local   Switch to local machine branch"
	echo "  u, update  Pull and rebase local machine branch"
	echo "  r, rebase  Interactively rebase machine branch"
	exit 0
else
	case="$1"
fi

# master
if [ "$case" -le "0" ]; then
	echo "=== (0) Stashing local changes of machine branch..."
	git stash
fi
if [ "$case" -le "1" ]; then
	printf "\n=== (1) Checking out local master branch...\n"
	git checkout master
fi
if [ "$case" -le "2" ]; then
	printf "\n=== (2) Pulling from remote...\n"
	git pull
fi
if [ "$case" -le "3" ]; then
	printf "\n=== (3 last) Applying local changes...\n"
	git stash apply
	printf "\n=== Done\n"
	echo "Add files with 'git add'"
	exit 0
fi

# commit
if [ "$case" -le "10" ]; then
	echo "=== (10) Committing staged changes..."
	git commit -v
fi
if [ "$case" -le "11" ]; then
	printf "\n=== (11) Pushing commit to remote master branch...\n"
	git push
fi
if [ "$case" -le "12" ]; then
	printf "\n=== (12) Stashing remaining local changes...\n"
	git stash
fi
if [ "$case" -le "13" ]; then
	printf "\n=== (13) Checking out local machine branch...\n"
	git checkout "$machine_branch"
fi
if [ "$case" -le "14" ]; then
	printf "\n=== (14) Applying changes from master branch to local machine branch...\n"
	git rebase origin/master
fi
if [ "$case" -le "15" ]; then
	printf "\n=== (15) Force pushing local machine branch to remote machine branch...\n"
	git push --force-with-lease origin "$machine_branch"
fi
if [ "$case" -le "16" ]; then
	printf "\n=== (16 last) Applying remaining local changes to local machine branch\n"
	git stash apply
	printf "\n=== Done\n"
	exit 0
fi

# local
if [ "$case" -le "20" ]; then
	echo "=== (20) Stashing local changes of master branch..."
	git stash
fi
if [ "$case" -le "21" ]; then
	printf "\n=== (21) Checking out local machine branch...\n"
	git checkout "$machine_branch"
fi
if [ "$case" -le "22" ]; then
	printf "\n=== (22 last) Applying local changes...\n"
	git stash apply
	printf "\n=== Done\n"
	echo "Add files with 'git add'"
	exit 0
fi

# update
if [ "$case" -le "30" ]; then
	echo "=== (30) Stashing local changes of master branch..."
	git stash
fi
if [ "$case" -le "31" ]; then
	printf "\n=== (31) Checking out local master branch...\n"
	git checkout master
fi
if [ "$case" -le "32" ]; then
	printf "\n=== (32) Pulling from remote...\n"
	git pull
fi
if [ "$case" -le "33" ]; then
	printf "\n=== (33) Checking out local machine branch...\n"
	git checkout "$machine_branch"
fi
if [ "$case" -le "34" ]; then
	printf "\n=== (34) Applying changes from master branch to local machine branch...\n"
	git rebase origin/master
fi
if [ "$case" -le "35" ]; then
	printf "\n=== (35) Force pushing local machine branch to remote machine branch...\n"
	git push --force-with-lease origin "$machine_branch"
fi
if [ "$case" -le "36" ]; then
	printf "\n=== (36 last) Applying local changes to local machine branch...\n"
	git stash apply
	printf "\n=== Done\n"
	exit 0
fi

# rebase
if [ "$case" -le "40" ]; then
	echo "=== (40) Stashing local changes of local machine branch..."
	git stash
fi
if [ "$case" -le "41" ]; then
	echo "=== (41) Rebasing machine branch..."
	first_machine_branch_commit="$(git log master.."$machine_branch" --oneline --no-abbrev | tail -1 | awk '{print $1}')"
	echo "First commit on machine branch has hash $first_machine_branch_commit"
	git rebase --interactive "$first_machine_branch_commit"^
fi
if [ "$case" -le "42" ]; then
	printf "\n=== (42 last) Applying local changes...\n"
	git stash apply
	printf "\n=== Done\n"
	exit 0
fi
