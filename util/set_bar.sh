#!/bin/sh

tmp_dir="/tmp/bar"
cache_dir="$HOME/.cache/bar"

# Gets current monitor
monitor="$(bspc query -T -m | jq -r .name)"
mkdir -p "$tmp_dir/$monitor"

color_file="$tmp_dir/$monitor/color"
fullscreen_flag_file="$tmp_dir/$monitor/fullscreen"
monocle_flag_file="$tmp_dir/$monitor/monocle"
update_flag_file="$tmp_dir/$monitor/update"
dark_mode_flag_file="$HOME/.cache/darkmode"

polybar_folder="$HOME/.config/polybar"
create_theme_script="$polybar_folder/create_theme.sh"
start_polybar_script="$polybar_folder/start_polybar.sh"

if [ -e "$dark_mode_flag_file" ]; then
	dark_mode="dark"
else
	dark_mode="light"
fi

# Checks if current window is fullscreened
if [ "$(bspc query -T -n | jq -r .client.state)" = "fullscreen" ]; then
	echo "Current window is fullscreened"

	# Checks if current window was fullscreened before or should update because of a signal from outside
	if [ ! -e "$fullscreen_flag_file" ] || [ -e "$update_flag_file" ]; then
		echo "Current window was not fullscreened before or should update"

		# Removes update signal from outside
		rm -f "$update_flag_file"

		# Removes bar
		"$start_polybar_script" "$monitor" NONE

		# Saves fullscreen state
		touch "$fullscreen_flag_file"
	fi
else
	if [ -e "$fullscreen_flag_file" ]; then
		touch "$update_flag_file"
		rm "$fullscreen_flag_file"
	fi

	# Checks if desktop is currently in tiled layout
	if [ "$(bspc query -T -d | jq -r .layout)" = "tiled" ]; then
		echo "Desktop is in tiling layout"

		# Checks if desktop was in monocle layout before or should update because of a signal from outside
		if [ -e "$monocle_flag_file" ] || [ -e "$update_flag_file" ]; then
			echo "Desktop was in monocle layout before or should update"

			# Removes update signal from outside
			rm -f "$update_flag_file"

			# Sets bar color to the default color
			"$start_polybar_script" "$monitor"

			# Saves layout
			rm -f "$monocle_flag_file"
		fi
	else
		echo "Desktop is in monocle layout"

		# Checks if a program runs
		if ! bspc query -T -n &> /dev/null; then
			color="monocle"
		else
			# Gets program name of current window
			program="$(xprop -id "$(xprop -root 32x '\t$0' _NET_ACTIVE_WINDOW | cut -f 2)" WM_CLASS | awk -F '"' '{print $2}')"
			program_folder="$cache_dir/$program"
			mkdir -p "$program_folder"
			program_darkmode_folder="$program_folder/$dark_mode"
			mkdir -p "$program_darkmode_folder"

			# Gets color of current program
			program_color_file="$program_darkmode_folder/color"
			if [ -e "$program_color_file" ]; then
				color=$(cat "$program_color_file")
			else
				echo "Getting color of program from screen"

				# Gets color from screen
				sleep 0.3
				color="$(maim -i "$(xdotool getactivewindow)" -g 1x1+100+0 | convert - txt:- | tail -n 1 | awk -F " " '{print $3}')"

				# Saves color
				echo "$color" > "$program_color_file"

				# Creates new theme if necessary
				if [ ! -e "$polybar_folder/theme_$color" ]; then
					echo "Creating theme for color"

					"$create_theme_script" "$color"
				fi
			fi
		fi

		# Gets old color of the bar
		old_color=$(cat "$color_file")

		# Checks if desktop was in tiled layout before or the color has changed or should update because of a signal from outside
		if [ ! -e "$monocle_flag_file" ] || [ "$old_color" != "$color" ] || [ -e "$update_flag_file" ]; then
			echo "Desktop was in tiling layout before or color has changed or should update"

			# Removes update signal from outside
			rm -f "$update_flag_file"

			# Sets bar color according to the program color
			"$start_polybar_script" "$monitor" "$polybar_folder/theme_$color"

			# Saves color and layout
			touch "$monocle_flag_file"
			echo "$color" > "$color_file"
		fi
	fi
fi
