#!/bin/sh

if [ -e /tmp/click1.lock ]; then
	exit 0
fi

echo $$ > /tmp/click1.lock

xdotool click 1

sleep 0.1

rm /tmp/click1.lock
