#!/bin/sh

if [ -e /tmp/click2.lock ]; then
	exit 0
fi

echo $$ > /tmp/click2.lock

xdotool click 2

sleep 0.1

rm /tmp/click2.lock
