#!/bin/sh

if [ -e /tmp/pgdn.lock ]; then
	exit 0
fi

echo $$ > /tmp/pgdn.lock

xdotool key Page_Down

sleep 0.1

rm /tmp/pgdn.lock
