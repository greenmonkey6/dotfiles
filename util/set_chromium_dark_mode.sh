#!/bin/sh

sleep 0.5

xdotool key Alt+Shift+d

sleep 0.5

if [ "$1" = "light" ]; then
	chromium "https://chrome.google.com/webstore/detail/material-incognito-light/necpbhkfondbpbloppmkjpdkdimldobc"
elif [ "$1" = "dark" ]; then
	chromium "https://chrome.google.com/webstore/detail/material-incognito-dark-t/ahifcnpnjgbadkjdhagpfjfkmlapfoel"
fi
