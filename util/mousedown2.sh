#!/bin/sh

if [ -e /tmp/mousedown2.lock ]; then
	exit 0
fi

echo $$ > /tmp/mousedown2.lock

xdotool mousedown 2

sleep 0.1

rm /tmp/mousedown2.lock
