#!/bin/sh

if [ "$1" == "" ]; then
	echo "Missing argument"
	exit 1
fi

ratio="$1"

if [ "$ratio" = "0" ]; then
	bspc config -d focused left_padding 0
	bspc config -d focused right_padding 0
else
	screen_res="$("$HOME/.config/util/get_screen_res.sh")"
	screen_width="${screen_res/x*}"
	screen_height="${screen_res/*x}"

	scaled_screen_width="$((screen_height * ratio / 100))"
	padding="$(((screen_width - scaled_screen_width) / 2))"

	bspc config -d focused left_padding "$padding"
	bspc config -d focused right_padding "$padding"
fi
