#!/usr/bin/python3

import sys
import re
import os


IGNORE_NUMBER = -1
IGNORE_NUMBER_STRING = "/"
DP_COMMENT_REGEX = "^(\"|;|#|!)( dp:([\.0-9]+f?|" + IGNORE_NUMBER_STRING + "))+$"


def main(argv):
    inputPath = argv[1]
    DP_SCALE_FACTOR = float(argv[2])

    inputFile = open(inputPath, "r")

    if not os.path.isfile(inputPath):
        print("File does not exist: " + inputPath)
        exit()

    # Reads and processes input file
    outputText = ""
    numbersRead = False
    numbers = []
    numbersFloat = []
    for line in inputFile:
        if numbersRead:
            # Splits up line into words of numbers and other chars
            words = re.findall(r"[^\.0-9]|[\.0-9]+", line)

            # Replaces numbers in line with numbers from the number list
            numberIndex = 0
            line = ""
            for word in words:
                if numberIndex < len(numbers) and ((not numbersFloat[numberIndex] and re.match("[0-9]+", word)) or (numbersFloat[numberIndex] and re.match("[\.0-9]+", word))):
                    if numbers[numberIndex] != IGNORE_NUMBER:
                        if numbersFloat[numberIndex]:
                            word = str(numbers[numberIndex])
                        else:
                            word = str(int(numbers[numberIndex]))
                    numberIndex += 1
                line += word

            numbers = []
            numbersFloat = []
            numbersRead = False
        else:
            if re.match(DP_COMMENT_REGEX, line):
                # Splits up line into words
                words = line.split()

                # Removes comment symbol from word list
                words.pop(0)

                # Puts dp number from word into number list
                for word in words:
                    dpStr = re.sub(r"dp:", "", word)
                    isFloat = False
                    if dpStr.endswith("f"):
                        isFloat = True
                        dpStr = re.sub(r"f", "", dpStr)
                    if dpStr == IGNORE_NUMBER_STRING:
                        dp = IGNORE_NUMBER
                    else:
                        if isFloat:
                            dp = float(dpStr)
                        else:
                            dp = round(float(dpStr))
                        # Multiplies dp number with dp scale factor
                        dp = float(dp * DP_SCALE_FACTOR)
                    numbers.append(dp)
                    numbersFloat.append(isFloat)

                numbersRead = True
        outputText += line
    inputFile.close()

    # Prints result to file
    outputFile = open(inputPath, "w")
    outputFile.write(outputText)
    outputFile.close()


if __name__ == "__main__":
    main(sys.argv)
