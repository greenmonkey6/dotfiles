#!/bin/sh

if [ -e /tmp/mousedown1.lock ]; then
	exit 0
fi

echo $$ > /tmp/mousedown1.lock

xdotool mousedown 1

sleep 0.1

rm /tmp/mousedown1.lock
