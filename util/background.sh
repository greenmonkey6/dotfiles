#!/bin/sh

# Sets resolution and path
root="$HOME/Pictures/Backgrounds"
dir="$root/Isle_of_Paradise"

old_file=""
while true; do
	if [ -e /tmp/work_mode ]; then
		file="$root"/work_mode.png
	else
		# Chooses image for right day time
		if [ "$(date "+%H%M")" -ge 1700 ]; then
			file="$dir"/evening.png
		elif [ "$(date "+%H%M")" -ge 1200 ]; then
			file="$dir"/noon.png
		elif [ "$(date "+%H%M")" -ge 600 ]; then
			file="$dir"/morning.png
		else
			file="$dir"/night.png
		fi
	fi

	# Sets image as background
	if [ "$file" != "$old_file" ]; then
		"$HOME"/.config/util/set_background.sh "$file"

		old_file=$file
	fi

	sleep 60
done
