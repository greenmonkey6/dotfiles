#!/bin/sh

if [ -e /tmp/toggle_mousemode.lock ]; then
	exit 0
fi

echo $$ > /tmp/toggle_mousemode.lock

if [ -e /tmp/mousemode.lock ]; then
	xset r rate 250 25
	rm /tmp/mousemode.lock
else
	xset r rate 1 50
	echo $$ > /tmp/mousemode.lock
fi

sleep 0.1
rm /tmp/toggle_mousemode.lock
