#!/bin/sh

if [ -e /tmp/mousedown3.lock ]; then
	exit 0
fi

echo $$ > /tmp/mousedown3.lock

xdotool mousedown 3

sleep 0.1

rm /tmp/mousedown3.lock
