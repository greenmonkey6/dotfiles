#!/bin/sh

bspc subscribe desktop node_focus node_state node_remove | xargs --max-lines=1 "$HOME"/.config/util/set_bar.sh
