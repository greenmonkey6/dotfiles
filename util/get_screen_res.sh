#!/bin/sh

res=$(xrandr | grep -E " connected primary [0-9]+x[0-9]+\+[0-9]+\+[0-9]+ " | awk -F '+' '{print $1}' | awk -F ' ' '{print $NF}')

if [ "$res" ]; then
	echo "$res" > /tmp/last_res
	echo "$res"
else
	cat /tmp/last_res
fi
