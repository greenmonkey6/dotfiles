#!/bin/sh

# Kills bar
killall -q polybar
bspc config top_padding 0

# Sets margins and gutters to 0 dp
bspc config window_gap 0
