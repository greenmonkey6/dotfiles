#!/bin/sh

# Starts bar
"$HOME"/.config/polybar/start_polybar.sh

# Sets margins and gutters to 16 dp as spedified in
# https://material.io/guidelines/components/cards.html#cards-content-blocks
# dp:16
bspc config window_gap 16
