#!/bin/sh

tmp_dir="/tmp" # TODO
mkdir -p "$tmp_dir"
cache_dir="$HOME/.cache/background"
mkdir -p "$cache_dir"

if [ ! $# -eq 1 ] && [ ! $# -eq 2 ]; then
	echo "Wrong number of arguments!"
	exit 1
fi

if [ $# -eq 2 ]; then
	if [ "$2" = "--pregen" ]; then
		pregen=true
	else
		echo "Unknown argument: $2"
	fi
fi

if [ "$1" = "--reload" ]; then
	if [ -e "$tmp_dir/last_background_input.png" ]; then
		input="$tmp_dir/last_background_input.png"
	else 
		echo "no last background to reload"
		exit 1
	fi
else
	ln -s -f "$1" "$tmp_dir/last_background_input.png"
	input="$1"
fi

# Gets resolution
res=$("$HOME"/.config/util/get_screen_res.sh)
width=$(echo "$res" | awk -F '[x]' '{print $1}')
height=$(echo "$res" | awk -F '[x]' '{print $2}')
echo res: "$width"x"$height"

res_cache_dir="$cache_dir/$res"
mkdir -p "$res_cache_dir"

if [ ! -e "$tmp_dir/pregen" ] || [ "$pregen" = true ]; then
	echo "pregen"

	# Creates gradient if necessary
	if [ ! -e "$tmp_dir/gradient.png" ] || [ "$(file "$tmp_dir/gradient.png" | awk -F ' ' '{print $5}')" -ne "$width" ]; then
		echo "gradient"

		gradient_cache_file="$cache_dir/$res/gradient.png"

		if [ ! -e "$gradient_cache_file" ]; then
			echo "gen gradient"

			gradient_height=$((width / 10))
			convert -size "$width"x"$gradient_height" gradient:'#00000065'-'#00000000' -sigmoidal-contrast 6,50% "$gradient_cache_file"

			# Adds dithering to the gradient (to work better with dark images)
			convert "$gradient_cache_file" -dither Riemersma -colors 16 "$gradient_cache_file"
		fi

		cp "$gradient_cache_file" "$tmp_dir/gradient.png"
	fi

	# Backup old background
	if [ -e "$tmp_dir/background.png" ]; then
		mv "$tmp_dir/background.png" "$tmp_dir/old_background.png"
	else
		# XXX: Bug in imagemagick does not properly handle xc:black
		convert -size "$width"x"$height" xc:#000001 \
			"$tmp_dir/old_background.png"
	fi
	transition=true

	# Set input image to monitor size
	convert -size "$width"x"$height" xc:black \
		"$input" -gravity Center -composite \
		"$tmp_dir/background.png"

	# Adds gradient to input image
	convert "$tmp_dir/background.png" -gravity Center \
		"$tmp_dir/gradient.png" -gravity North -composite \
		"$tmp_dir/background.png"

	# Generates a transition between the backgrounds
	if [ "$transition" = true ]; then
		echo "pregen transition"
		composite -dissolve 10 "$tmp_dir/background.png" "$tmp_dir/old_background.png" "$tmp_dir/transition1.jpg"
		composite -dissolve 35 "$tmp_dir/background.png" "$tmp_dir/old_background.png" "$tmp_dir/transition2.jpg"
		composite -dissolve 65 "$tmp_dir/background.png" "$tmp_dir/old_background.png" "$tmp_dir/transition3.jpg"
		composite -dissolve 90 "$tmp_dir/background.png" "$tmp_dir/old_background.png" "$tmp_dir/transition4.jpg"
		composite -dissolve 100 "$tmp_dir/background.png" "$tmp_dir/old_background.png" "$tmp_dir/transition5.jpg"
		touch "$tmp_dir/background_transition"
	fi
fi

if [ ! "$pregen" = true ]; then
	echo "draw"
	rm -f "$tmp_dir/pregen"

	# Displays transition
	if [ -e "$tmp_dir/background_transition" ]; then
		echo "draw transition"
		feh --bg-center "$tmp_dir/transition1.jpg"
		feh --bg-center "$tmp_dir/transition2.jpg"
		feh --bg-center "$tmp_dir/transition3.jpg"
		feh --bg-center "$tmp_dir/transition4.jpg"
		feh --bg-center "$tmp_dir/transition5.jpg"
		rm "$tmp_dir/background_transition"
	fi
	feh --bg-center "$tmp_dir/background.png"
else
	touch "$tmp_dir/pregen"
fi
