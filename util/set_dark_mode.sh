#!/bin/sh

flag_file="$HOME/.cache/darkmode"

if [ "$1" = "toggle" ]; then
	if [ -e "$flag_file" ]; then
		mode="light"
	else
		mode="dark"
	fi
else
	mode="$1"
fi

if [ "$2" = "all" ]; then
	amount="all"
else
	amount="minimal"
fi


if [ "$mode" = "light" ]; then
	if [ -e "$flag_file" ]; then
		# Termite
		ln -s -f "$HOME/Documents/dotfiles/termite/config" "$HOME/.config/termite/config"
		killall -USR1 termite

		# Rofi
		ln -s -f "$HOME/Documents/dotfiles/rofi/config" "$HOME/.config/rofi/config"

		# Zathura
		ln -s -f "$HOME/Documents/dotfiles/zathura/zathurarc" "$HOME/.config/zathura/zathurarc"

		# GTK
		ln -s -f "$HOME/Documents/dotfiles/gtk/settings.ini" "$HOME/.config/gtk-3.0/settings.ini"
		ln -s -f "$HOME/Documents/dotfiles/gtk/.gtkrc-2.0" "$HOME/.gtkrc-2.0"

		if [ "$amount" = "all" ]; then
			# Chromium
			"$HOME/.config/util/set_chromium_dark_mode.sh" light
		fi

		# Spotify
		spicetify config current_theme MaterialLight
		spicetify update

		rm -f "$flag_file"
	else
		echo "Already in light mode"
	fi
elif [ "$mode" = "dark" ]; then
	if ! [ -e "$flag_file" ]; then
		# Termite
		ln -s -f "$HOME/Documents/dotfiles/termite/configDark" "$HOME/.config/termite/config"
		killall -USR1 termite

		# Rofi
		ln -s -f "$HOME/Documents/dotfiles/rofi/configDark" "$HOME/.config/rofi/config"

		# Zathura
		ln -s -f "$HOME/Documents/dotfiles/zathura/zathurarcDark" "$HOME/.config/zathura/zathurarc"

		# GTK
		ln -s -f "$HOME/Documents/dotfiles/gtk/settingsDark.ini" "$HOME/.config/gtk-3.0/settings.ini"
		ln -s -f "$HOME/Documents/dotfiles/gtk/.gtkrc-2.0Dark" "$HOME/.gtkrc-2.0"

		if [ "$amount" = "all" ]; then
			# Chromium
			"$HOME/.config/util/set_chromium_dark_mode.sh" dark
		fi

		# Spotify
		spicetify config current_theme MaterialDark
		spicetify update

		touch "$flag_file"
	else
		echo "Already in dark mode"
	fi
else
	echo "Unknown mode: $mode"
fi
