#!/bin/sh

dpi="$1"

find . -type f -regex "\./[^\.].*" -exec util/setdpi.py {} "$dpi" \;

echo "$dpi" > util/dpi
