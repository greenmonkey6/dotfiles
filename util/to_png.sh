#!/bin/sh

# Gets page geometry
page_width=$(grep width "$1" | head -n 1 | awk -F '"' '{print $2}')
page_height=$(grep height "$1" | head -n 1 | awk -F '"' '{print $2}')

# Gets user's geometry
width=$(echo "$2" | awk -F 'x' '{print $1}')
height=$(echo "$2" | awk -F 'x' '{print $2}')

# XXX Assumes given geometry is wider than page's
scaled_width=$(((width * page_height) / height))
offset=$(((page_width - scaled_width) / 2))

# Exports image
inkscape --export-png="${1%.*}".png \
	--export-area="$offset:0:$((scaled_width + offset)):$page_height" \
	--export-width="$width" --export-height="$height" \
	"$1"
