#!/bin/sh

if [ -e /tmp/pgup.lock ]; then
	exit 0
fi

echo $$ > /tmp/pgup.lock

xdotool key Page_Up

sleep 0.1

rm /tmp/pgup.lock
