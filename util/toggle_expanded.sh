#!/bin/sh

tmp_dir="/tmp/bar"
monitor="$(bspc query -T -m | jq -r .name)"
mkdir -p "$tmp_dir/$monitor"

update_flag_file="$tmp_dir/$monitor/update"
expanded_flag_file="$tmp_dir/$monitor/expanded"

if [ -e "$expanded_flag_file" ]; then
	rm "$expanded_flag_file"
else
	touch "$expanded_flag_file"
fi

# Signals the set_bar.sh script to restart the bar the next time its called
touch "$update_flag_file"
