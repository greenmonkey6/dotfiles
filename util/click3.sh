#!/bin/sh

if [ -e /tmp/click3.lock ]; then
	exit 0
fi

echo $$ > /tmp/click3.lock

xdotool click 3

sleep 0.1

rm /tmp/click3.lock
