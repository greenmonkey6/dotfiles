food="$(ssh ella cat track/nutrients.txt | cut -d ' ' -f 1 | tail -n +2 | grep ".\+" | rofi -dmenu -i -sorting fzf)"
if [ "$?" != 0 ]; then
	exit 1
fi
amount="$(rofi -dmenu)"
if [ "$?" != 0 ]; then
	exit 1
fi

file="$(ssh ella track/edit.sh -n)"
ssh ella "echo \"$food $amount\" >> \"track/$file\""
