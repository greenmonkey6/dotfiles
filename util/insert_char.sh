#!/bin/sh

dotfiles_folder="$HOME/Documents/dotfiles"

selector="rofi -dmenu -i -matching fuzzy -sort -sorting-method fzf"
#selector="fzf"
output="xclip -selection clipboard"
#output="cat"

mode="$(printf "digraph\nicon\ncolor" | $selector)"

if [ "$mode" = "icon" ]; then
	cat "$dotfiles_folder/util/material_icon_names.txt" \
		| cut -d '	' -f 3,4 \
		| $selector \
		| cut -d '	' -f 1 \
		| tr -d '\n' \
		| $output
elif [ "$mode" = "digraph" ]; then
	cat /usr/share/vim/vim82/doc/digraph.txt \
		| grep -E '^[^	]*	[^	]{2}	[0-9a-fA-Fx]{4}	[^	]+	[^	]+' \
		| awk -F '	' '{print $1 "\t" $2 "\t" $5}' \
		| $selector \
		| cut -d '	' -f 1 \
		| tr -d '\n' \
		| $output
elif [ "$mode" = "color" ]; then
	cat "$dotfiles_folder/util/material_colors.txt" \
		| $selector \
		| awk '{print $NF}' \
		| sed 's/\#//' \
		| tr -d '\n' \
		| $output
else
	exit 1
fi
