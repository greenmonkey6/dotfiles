#!/bin/sh

if [ "$1" = "get" ]; then
	ssh -q ella "cat phone_notifications" | jq -r '.[] | select(.packageName == "nodomain.freeyourgadget.gadgetbridge" and .title == "Hybrid HR Collider") | .content'
elif [ "$1" = "show" ]; then
	echo "$2" | grep -q "Verbunden" || echo ""
elif [ "$1" = "showFull" ]; then
	echo "$2" | grep -o "[0-9]\+\%" | xargs -i echo " {}" | grep -x ".*" || echo ""
fi
