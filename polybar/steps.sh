#!/bin/sh

if [ "$1" = "get" ]; then
	ssh -q ella "cat phone_notifications" | jq -r '.[] | select(.packageName == "de.j4velin.pedometer") | .title' | grep -o '[0-9\.]\+'
elif [ "$1" = "show" ]; then
	:
elif [ "$1" = "showFull" ]; then
	(echo "$2" | grep -x ".\+" || echo "?") | xargs -i echo " {}"
fi
