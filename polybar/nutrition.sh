#!/bin/sh

if [ "$1" = "get" ]; then
	ssh -q ella "cd track && ./sum.sh -e"
	ssh -q ella "cd track && ./sum.sh"
elif [ "$1" = "show" ]; then
	enough="$(echo "$2" | head -n 1)"
	if ! [ "$enough" = true ]; then
		echo ""
	fi
elif [ "$1" = "showFull" ]; then
	nutrients="$(echo "$2" | tail -n 1)"
	echo " $nutrients"
fi
