#!/bin/sh

#

get_number() {
	if [ "$1" = 1 ]; then
		number=""
	elif [ "$1" = 2 ]; then
		number="²"
	elif [ "$1" = 3 ]; then
		number="³"
	elif [ "$1" = 4 ]; then
		number="⁴"
	else
		number="+"
	fi
}


get_status_youtube() {
	raw_status="$(curl --silent "$1" | grep -o '\"playabilityStatus\"\:{\"status\"\:\"[^\"]*\"' | grep -o '[^\"]*' | tail -n 1)"
	if [ "$?" != 0 ]; then
		status="offline"
	else
		if [ "$raw_status" = "OK" ]; then
			if youtube-dl -s -q "$1" 2> /dev/null; then
				status="live"
			else
				status="active"
			fi
		else
			status="unknown: \"$raw_status\""
		fi
	fi
}


get_status_twitch() {
	if youtube-dl -s -q "$1" 2> /dev/null; then
		status="live"
	else
		status="offline"
	fi
}


add_link() {
	name="$1"
	link="$2"

	if echo "$link" | grep -q "youtube"; then
		platform="youtube"
		platform_icon=""
	elif echo "$link" | grep -q "twitch"; then
		platform="twitch"
		platform_icon=""
	else
		platform="unknown"
		platform_icon=""
	fi

	if [ "$platform" = "youtube" ]; then
		get_status_youtube "$link"

		if [ "$state_youtube" != "live" ] && [ "$status" = "live" ]; then
			state_youtube="live"
		elif [ "$state_youtube" != "active" ] && [ "$status" = "active" ]; then
			state_youtube="active"
		fi
		if [ "$status" = "live" ] || [ "$status" = "active" ]; then
			count_youtube="$((count_youtube + 1))"
		fi
	elif [ "$platform" = "twitch" ]; then
		get_status_twitch "$link"

		if [ "$state_twitch" != "live" ] && [ "$status" = "live" ]; then
			state_twitch="live"
		elif [ "$state_twitch" != "active" ] && [ "$status" = "active" ]; then
			state_twitch="active"
		fi
		if [ "$status" = "live" ] || [ "$status" = "active" ]; then
			count_twitch="$((count_twitch + 1))"
		fi
	else
		status="unknown platform: \"$platform\""
	fi

	if [ "$state" != "live" ] && [ "$status" = "live" ]; then
		state="live"
	elif [ "$state" != "active" ] && [ "$status" = "active" ]; then
		state="active"
	fi

	if [ "$status" = "active" ]; then
		if [ "$text" != "" ]; then text="$text "; fi
		text="$text$platform_icon $name"
	elif [ "$status" = "live" ]; then
		if [ "$text" != "" ]; then text="$text "; fi
		text="$text$platform_icon $name"
	fi
}

if [ "$1" = "get" ]; then
	state="offline"
	state_youtube="offline"
	state_twitch="offline"
	count_youtube=0
	count_twitch=0
	text=""

	add_link "ungespielt" "https://www.youtube.com/c/ungespielt/live"
	add_link "LetsGameDev" "https://www.youtube.com/c/Tomzalat/live"
	add_link "ilmango" "https://twitch.tv/ilmango"
	add_link "Dekarldent" "https://twitch.tv/Dekarldent"
	add_link "docm77" "https://twitch.tv/docm77LIVE"
	add_link "Rezo" "https://twitch.tv/rezo"
	add_link "Reved" "https://www.twitch.tv/revedtv"
	add_link "MiiMii" "https://www.twitch.tv/miistream"
	add_link "PascalSu" "https://www.twitch.tv/pascalsu"

	echo "$state"
	echo "$state_youtube"
	echo "$state_twitch"
	echo "$count_youtube"
	echo "$count_twitch"
	echo "$text"
elif [ "$1" = "show" ]; then
	state_youtube="$(echo "$2" | head -n 2 | tail -n 1)"
	state_twitch="$(echo "$2" | head -n 3 | tail -n 1)"
	count_youtube="$(echo "$2" | head -n 4 | tail -n 1)"
	count_twitch="$(echo "$2" | head -n 5 | tail -n 1)"

	get_number "$count_youtube"
	number_youtube="$number"
	get_number "$count_twitch"
	number_twitch="$number"

	text=""
	if ! [ -e /tmp/work_mode ]; then
		first=false
		if [ "$state_youtube" = "live" ]; then
			printf "$number_youtube"
			first=true
		elif [ "$state_youtube" = "active" ]; then
			printf "$number_youtube"
			first=true
		fi
		if [ "$state_twitch" = "live" ]; then
			if [ "$first" = true ]; then printf " "; fi
			printf "$number_twitch"
		fi
		printf "\n"
	fi
elif [ "$1" = "showFull" ]; then
	state="$(echo "$2" | head -n 1)"
	text="$(echo "$2" | tail -n 1)"

	if [ "$state" = "live" ] || [ "$state" = "active" ]; then
		echo "$text"
	else
		echo ""
	fi
fi
