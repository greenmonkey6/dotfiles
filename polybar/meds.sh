#!/bin/sh

if [ "$1" = "get" ]; then
	ssh -q ella "cat phone_notifications" | jq -r '.[] | select(.packageName == "com.github.quarck.calnotify") | .title' | grep -x "\(Tablette\|Vitamin B12\|Vitamin D\)"
elif [ "$1" = "show" ]; then
	if [ "$2" != "" ]; then
		echo ""
	fi
elif [ "$1" = "showFull" ]; then
	(echo "$2" | tr '\n' ';' | sed 's/;*$//;s/;/, /g' | grep -x ".*" || echo "") | xargs -i echo " {}"
fi
