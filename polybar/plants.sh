#!/bin/sh

if [ "$1" = "get" ]; then
	ssh -q ella "cat phone_notifications" | jq -r '[.[] | select(.packageName == "org.isoron.uhabits") | .title == "Pflanzen"] | any'
elif [ "$1" = "show" ]; then
	if [ "$2" = "true" ]; then
		echo ""
	fi
elif [ "$1" = "showFull" ]; then
	if [ "$2" = "true" ]; then
		echo ""
	fi
fi
