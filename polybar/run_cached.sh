#!/bin/sh

cache() {
	if [ ! -e "$lock_file" ]; then
		touch "$lock_file"
		out="$(sh "$script_file" "get" 2> /dev/null)"
		rm -f "$lock_file"
		touch "$write_lock_file"
		echo "$out" > "$cache_file"
		rm -f "$write_lock_file"
	fi
}


config_dir="$(dirname "$0")"
tmp_dir="/tmp/bar"

is_full="$2"
script_name="$1"
script_file="$config_dir/$script_name"
cache_file="$tmp_dir/$script_name.cache"
lock_file="$tmp_dir/$script_name.lock"
write_lock_file="$tmp_dir/$script_name.write_lock"

if ! [ -e "$cache_file" ]; then
	cache
fi

while [ -e "$write_lock_file" ]; do
	sleep 0.1
done

# 'echo' to print a new line if show/showFull prints nothing
if [ "$is_full" = true ]; then
	text="$(sh "$script_file" "showFull" "$(cat "$cache_file")" 2> /dev/null)"
else
	text="$(sh "$script_file" "show" "$(cat "$cache_file")" 2> /dev/null)"
fi

## Why does this error even happen? (Cache files seem to be fine)
#if echo "$text" | grep -q "Broken pipe"; then
#	echo ""
#	return
#fi

echo "$text"

cache &
