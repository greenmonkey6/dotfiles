#!/bin/sh

if [ "$1" = "get" ]; then
	ssh ella -q -o ConnectTimeout=1 cat notifications
elif [ "$1" = "show" ]; then
	if [ "$2" != "" ]; then
		echo ""
	fi
elif [ "$1" = "showFull" ]; then
	echo "$2" | tr '\n' ';' | sed 's/;*$//;s/;/, /g' | xargs -i echo " {}" | grep -x ".*" || echo ""
fi
