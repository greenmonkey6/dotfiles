[colors]
; Defines invisible color
invisible = ${env:POLYBAR_INVISIBLE}
; Sets background color to 20% black as specified for a dark status bar in
; https://material.io/guidelines/layout/structure.html#structure-system-bars
background = ${env:POLYBAR_BACKGROUND}
; Sets foreground color to 100% white as specified for white text on dark background in
; https://material.io/guidelines/style/color.html#color-usability
foreground = ${env:POLYBAR_FOREGROUND}
; Sets disabled foreground color to 50% white as specified for white text on dark background in
; https://material.io/guidelines/style/color.html#color-usability
disabled = ${env:POLYBAR_DISABLED}
; Sets alert color as used on android in low battery symbol (Deep Orange 600
; in https://material.io/guidelines/style/color.html#color-color-palette)
alert = ${env:POLYBAR_ALERT}

[bar/base]
enable-ipc = true

monitor = ${env:POLYBAR_MONITOR}

; Sets bar height as used in android specified in
; https://material.io/guidelines/layout/structure.html#structure-system-bars
; dp:24
height = 24

; Sets padding on both sides to approximately 8dp as used on android, visible in
; https://material.io/guidelines/layout/metrics-keylines.html
padding-left = 2
padding-right = 2

; Sets margins to approximately match those of android
module-margin-left = 1
module-margin-right = 0

; Sets dpi for text
; dp:100
dpi = 100
; Sets typefaces as specified in https://material.io/guidelines/style/typography.html
; and https://material.io/guidelines/style/icons.html#icons-system-icons.
; Size and offset is approximately as on android
; dp:/ dp:/ dp:3
font-0 = Roboto Medium:pixelsize=10.5;3
; dp:/ dp:/ dp:3
font-1 = Material Design Icons:pixelsize=13;3
; dp:/ dp:/ dp:3
font-2 = Roboto Mono Medium for Powerline:pixelsize=10.5;3

; Sets background and foreground colors as specified above
background = ${colors.background}
foreground = ${colors.foreground}

; Disables system tray
tray-position = none

[bar/minimal]
inherit = bar/base

; Sets desktop names to the left and system info to the right
modules-left = bspwm
modules-center =
modules-right = volume eth wlan battery date

[bar/full]
inherit = bar/base

tray-position = right

module-margin-left = 5

; Sets desktop names to the left and system info to the right
modules-left = bspwm
modules-center =
modules-right = xkeyboard volumeFull ethFull wlanFull batteryFull dateFull

[module/bspwm]
type = internal/bspwm

; Lets workspaces only show up on the same output as the bar
pin-workspaces = true

label-focused = %name%
label-focused-foreground = ${colors.foreground}
label-focused-padding = 0

label-occupied = %name%
label-occupied-foreground = ${colors.disabled}
label-occupied-padding = 0

label-urgent = %name%
label-urgent-foreground = ${colors.alert}
label-urgent-padding = 0

; Lets empty workspaces be hidden
label-empty =

[module/wlan]
type = internal/network
interface =
interval = 3.0

format-disconnected = <label-disconnected>
label-disconnected =

; This prefix symbol has width 0, so it gets basically painted behind the next symbol
format-connected-prefix = 蠟
format-connected-prefix-foreground = ${colors.disabled}

format-connected = <ramp-signal><label-connected>

label-connected =

ramp-signal-0 = 爛
ramp-signal-1 = 嵐
ramp-signal-2 = 襤
; XXX Hack fix
ramp-signal-3 = 蠟襤

animation-packetloss-0 = 廊

[module/wlanFull]
inherit = module/wlan

interval = 0.5

label-connected = " %essid% %{T3}%upspeed:8:8%%{T-} %{T3}%downspeed:8:8%%{T-}"

[module/eth]
type = internal/network
interface =
interval = 3.0

format-connected = <label-connected>
format-disconnected = <label-disconnected>
format-packetloss = <animation-packetloss>

# 
label-connected = 
label-disconnected =

animation-packetloss-0 = 

[module/ethFull]
inherit = module/eth

interval = 0.5

label-connected =  %{T3}%upspeed:8:8%%{T-} %{T3}%downspeed:8:8%%{T-}

[module/date]
type = internal/date
interval = 5

date =

time = %H:%M

label = %date%%time%

[module/dateFull]
inherit = module/date

date = "%A, %d.%m.%Y, "

[module/volume]
type = internal/pulseaudio

format-volume = <ramp-volume><label-volume>
label-volume =

# 
label-muted = 

ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

[module/volumeFull]
inherit = module/volume

label-volume = " %percentage%%"

[module/battery]
type = internal/battery

full-at = 99

battery = BAT0
adapter = ADP1

poll-interval = 5

; This prefix symbol has width 0, so it gets basically painted behind the next symbol
format-charging-prefix = 
format-charging-prefix-foreground = ${colors.disabled}

format-charging = <animation-charging><label-charging>
label-charging =

animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-framerate = 500

; This prefix symbol has width 0, so it gets basically painted behind the next symbol
format-full-prefix = 
format-full-prefix-foreground = ${colors.disabled}

format-full = <label-full>
label-full = 

; This prefix symbol has width 0, so it gets basically painted behind the next symbol
format-discharging-prefix = 
format-discharging-prefix-foreground = ${colors.disabled}

format-discharging = <ramp-capacity><label-discharging>
label-discharging =

ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-5 = 
ramp-capacity-6 = 
ramp-capacity-7 = 
ramp-capacity-8 = 
ramp-capacity-9 = 
; XXX Hack fix
ramp-capacity-10 = 

ramp-capacity-0-foreground = ${colors.alert}
ramp-capacity-1-foreground = ${colors.alert}

[module/batteryFull]
inherit = module/battery

label-charging = " %percentage%%"

label-discharging = " %percentage%%"

[module/xkeyboard]
type = internal/xkeyboard

blacklist-0 = num lock
blacklist-1 = scroll lock

format = <label-layout>
format-spacing = 0

label-layout =  %name%

[module/webcam]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh webcam.sh $POLYBAR_IS_FULL"
interval = 1

[module/mic]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh mic.sh $POLYBAR_IS_FULL"
interval = 1

[module/notifications]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh notifications.sh $POLYBAR_IS_FULL"
interval = 10

[module/steps]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh steps.sh $POLYBAR_IS_FULL"
interval = 10

[module/plants]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh plants.sh $POLYBAR_IS_FULL"
interval = 10

[module/training]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh training.sh $POLYBAR_IS_FULL"
interval = 10

[module/walking]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh walking.sh $POLYBAR_IS_FULL"
interval = 10

[module/vacuum]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh vacuum.sh $POLYBAR_IS_FULL"
interval = 10

[module/launder]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh launder.sh $POLYBAR_IS_FULL"
interval = 10

[module/meds]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh meds.sh $POLYBAR_IS_FULL"
interval = 10

[module/nutrition]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh nutrition.sh $POLYBAR_IS_FULL"
interval = 30

[module/mw]
type = custom/script
exec = "~/Documents/ModuleWorks/polybar.sh $POLYBAR_IS_FULL"
interval = 1

[module/work]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh work.sh $POLYBAR_IS_FULL"
interval = 30

[module/phone]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh phone.sh $POLYBAR_IS_FULL"
interval = 10

[module/ella]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh ella.sh $POLYBAR_IS_FULL"
interval = 10

[module/pi]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh pi.sh $POLYBAR_IS_FULL"
interval = 10

[module/modbert]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh modbert.sh $POLYBAR_IS_FULL"
interval = 10

[module/watch]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh watch.sh $POLYBAR_IS_FULL"
interval = 10

[module/record]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh record.sh $POLYBAR_IS_FULL"
interval = 1

[module/streams]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh streams.sh $POLYBAR_IS_FULL"
interval = 60

[module/mc]
type = custom/script
exec = "$POLYBAR_CONFIG_DIR/run_cached.sh mc.sh $POLYBAR_IS_FULL"
interval = 10

[settings]
screenchange-reload = true

[global/wm]
; Disables margins
margin-top = 0
margin-bottom = 0

; vim:filetype=dosini
