#!/bin/sh

config_dir="$(dirname "$0")"
cd "$config_dir"

if [ "$1" = "get" ]; then
	cd "$config_dir"

	if [ ! -e "MCList.class" ]; then
		javac "MCList.java"
	fi

	json="$(java MCList "mndmc.nitrado.at" 2> /dev/null)"
	if [ "$json" = "offline" ]; then
		echo "offline"
		return
	fi
	players="$(echo "$json" | jq -r '.players')"
	online="$(echo "$players" | jq -r '.online')"
	max="$(echo "$players" | jq -r '.max')"

	echo "$online/$max"
elif [ "$1" = "show" ]; then
	if ! [ -e /tmp/work_mode ]; then
		if ! echo "$2" | grep -xq "0/.*" && [ "$2" != "offline" ]; then
			echo ""
		fi
	fi
elif [ "$1" = "showFull" ]; then
	echo " $2"
fi
