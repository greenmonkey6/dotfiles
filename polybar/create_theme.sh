#!/bin/sh

if [ ! $# -eq 1 ]; then
	echo "Wrong number of arguments!"
	exit 1
fi

# Adds a bit light black to the input color for the background
convert -size 1x1 xc:"$1" /tmp/background.png
convert -size 1x1 xc:"#0000000A" /tmp/black.png
convert /tmp/background.png /tmp/black.png -composite /tmp/color.png
background=$(convert /tmp/color.png txt:- | tail -n 1 | awk -F " " '{print $3}')

RGB_8bit="$(convert /tmp/color.png -colorspace srgb txt:- | tail -n 1 | awk -F '[ ()]' '{print $3}')"
constrast_light="$(echo "contrast($RGB_8bit,255,255,255)" | bc ~/Documents/dotfiles/bc/color.bc -l)"
constrast_dark="$(echo "contrast($RGB_8bit,97,97,97)" | bc ~/Documents/dotfiles/bc/color.bc -l)"
echo "$constrast_light $constrast_dark"

# Creates light or dark theme based on contrast
if [ "$(echo "$constrast_dark > $constrast_light" | bc -l)" = "1" ]; then
	foreground="#616161"
	disabled="#7F616161"
else
	foreground="#FFFFFF"
	disabled="#7FFFFFFF"
fi

# Prints theme to file
out="$HOME/.config/polybar/theme_$1"
echo 'export POLYBAR_BACKGROUND='"'$background'" > "$out"
echo 'export POLYBAR_FOREGROUND='"'$foreground'" >> "$out"
echo 'export POLYBAR_DISABLED='"'$disabled'" >> "$out"
