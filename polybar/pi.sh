#!/bin/sh

if [ "$1" = "get" ]; then
	if ssh -q -o ConnectTimeout=1 pi exit; then
		echo "true"
	else
		echo "false"
	fi
elif [ "$1" = "show" ]; then
	if [ "$2" != "true" ]; then
		echo ""
	fi
elif [ "$1" = "showFull" ]; then
	if [ "$2" = "true" ]; then
		echo " pi"
	else
		echo " pi"
	fi
fi
