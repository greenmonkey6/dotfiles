#!/bin/sh

tmp_dir="/tmp/bar"
cache_dir="$HOME/.cache/bar"
config_dir="$HOME/.config/polybar"

# Gets current monitor
monitor="$(bspc query -T -m | jq -r .name)"
mkdir -p "$tmp_dir/$monitor"

monitor_folder="$tmp_dir/$monitor"
expanded_flag_file="$monitor_folder/expanded"

# Gets monitor
if [ $# -gt 0 ]; then
	monitor="$1"
else
	monitor="$(xrandr --listactivemonitors | tail -n +2 | head -n 1 | awk '{print $NF}')"
fi

# Saves IPC FIFOs for currently running bars on current monitor
if [ -e "$monitor_folder/" ]; then
	old_bars="$(find "$monitor_folder/" -type l -regex "$monitor_folder/polybar_mqueue\..*" -printf "%f\n")"
fi

if [ -e "$expanded_flag_file" ]; then
	is_full=true
else
	is_full=false
fi

# Exports for usage in custom scripts from polybar config
export POLYBAR_CONFIG_DIR="$config_dir"
export POLYBAR_TMP_DIR="$tmp_dir"
export POLYBAR_IS_FULL="$is_full"

if [ "$2" != "NONE" ]; then
	# Loads default theme
	. "$HOME"/.config/polybar/theme_default

	# Overrides parts of default theme with custom theme if given
	if [ $# -gt 1 ]; then
		. "$2"
	fi

	# Starts bar on given monitor
	export POLYBAR_MONITOR="$monitor"
	if [ "$is_full" = true ]; then
		bar_name="full"
	else
		bar_name="minimal"
	fi
	polybar "$bar_name" &
#	2> /dev/null &

	# Waits for polybar to start
	sleep 0.2

	# Makes symbolic link to IPC FIFO of started bar in current monitor folder
	mkdir -p "$monitor_folder"
	ln -s "/tmp/polybar_mqueue.$!" "$monitor_folder/polybar_mqueue.$!"
fi

# Kills old instances
for i in $old_bars; do
	echo "killing $i ..."
	echo "cmd:quit" > "$monitor_folder/$i"
	rm "$monitor_folder/$i"
	(sleep 0.5
	if [ -e "/tmp/$i" ]; then
		echo "force killing $i ..."
		pid="$(echo "$i" | grep -o "[0-9]*$")"
		kill -9 "$pid"
		rm -f "/tmp/$i"
	fi) &
done
