#!/bin/bash

# Changes color of foreground and background
setterm --foreground black --background white

# Black
echo -en "\e]P0000000"
echo -en "\e]P8212121"

# Red
echo -en "\e]P1F44336"
echo -en "\e]P9F44336"

# Green
echo -en "\e]P2009688"
echo -en "\e]PA009688"

# Yellow
echo -en "\e]P3FF5722"
echo -en "\e]PBFF5722"

# Blue
echo -en "\e]P42196F3"
echo -en "\e]PC2196F3"

# Magenta
echo -en "\e]P5E91E63"
echo -en "\e]PDE91E63"

# Cyan
echo -en "\e]P63F51B5"
echo -en "\e]PE3F51B5"

# White
echo -en "\e]P7E0E0E0"
echo -en "\e]PFE0E0E0"

# Clears screen
clear
