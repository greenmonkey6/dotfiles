#!/bin/sh

root="$HOME/Pictures/Backgrounds"
dir="$root/Isle_of_Paradise"

# Chooses image for right day time
if [ "$(date "+%H%M")" -ge 1300 ]; then
	file="$dir"/noon.png
elif [ "$(date "+%H%M")" -ge 1000 ]; then
	file="$dir"/morning.png
else
	file="$dir"/evening.png
fi

# Gets resolution
res=$("$HOME"/.config/util/get_screen_res.sh)

# Sets image as background (slightly darkened)
convert -size "$res" xc:black \
	"$file" -gravity Center -composite \
	-size "$res" xc:"#00000065" -composite \
	/tmp/background.png

# Pregenerates transition to new background
"$HOME"/.config/util/set_background.sh "$file" --pregen &
