#!/bin/sh

# Moves cursor to bottom right corner
xdotool mousemove 1000000 1000000

# Disables monitor standby
xset -dpms

# Saves screen res
/home/tobi/.config/util/get_screen_res.sh

# Sets background
sudo -H -u tobi /etc/lightdm/lightdm-set_background.sh > /dev/null
