#!/bin/sh

# Kills polybar
killall -q picom

# Waits for picom to close
while pgrep -x picom >/dev/null; do
	sleep 1;
done

# Starts picom
picom &
