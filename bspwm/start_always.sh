#!/bin/sh

# Sets up desktop

# Starts compositor
"$HOME"/.config/picom/start_picom.sh

bspc config remove_disabled_monitors true
bspc config remove_unplugged_monitors true

# Sets desktop names (requires Material Icon font from Google)
if [ "$(bspc query --monitors | wc -l)" = 1 ]; then
	bspc monitor any --reset-desktops " " " " " " " " " " " " " " " " " " " "
elif [ "$(bspc query --monitors | wc -l)" = 2 ]; then
	primary="$(bspc query -T -m primary | jq -r .name)"
	secondary="$(bspc query -T -m next | jq -r .name)"
	bspc wm --reorder-monitors "$primary" "$secondary"
	bspc monitor "$primary" --reset-desktops " " " " " " " " " "
	bspc monitor "$secondary" --reset-desktops " " " " " " " " " "
fi

# Starts bars on all monitors
bspc config top_padding 0
for monitor in $(xrandr --listactivemonitors | tail -n +2 | awk '{print $NF}'); do
	"$HOME"/.config/polybar/start_polybar.sh "$monitor"
done

# Sets margins and gutters to 16 dp as specified in
# https://material.io/guidelines/components/cards.html#cards-content-blocks
# dp:16
bspc config window_gap 16


# Sets up Xorg

# Sets key rate
xset r rate 250 25

# Sets right cursor
xsetroot -cursor_name left_ptr

# Disable monitor standby
xset -dpms


# Sets up bspwm

# Enables Mouse focusing
bspc config click_to_focus true

# Disables borders
bspc config border_width 0

# Sets split ratio
bspc config split_ratio 0.5

# Sets color of preselect
bspc config presel_feedback_color "#000000"

bspc config borderless_monocle true
bspc config gapless_monocle true

bspc rule -r '*:*'

# Lets zathura always start tiled, otherwise it would start floating
bspc rule -a Zathura state=tiled

# Lets all of godot start floating
# TODO Only let the window with the game start floating
bspc rule -a Godot state=floating

# Reloads all terminals
killall -USR1 termite

# Reloads hot key daemon
pkill -USR1 -x sxhkd
